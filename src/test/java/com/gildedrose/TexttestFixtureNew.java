package com.gildedrose;

import com.gildedrose.entity.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.util.StreamUtils;

import java.io.*;
import java.nio.charset.Charset;

public class TexttestFixtureNew {
    @Test
    public void main() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));

        String masterFile = TexttestFixtureNew.class.getClassLoader().getResource("golden-master-with-conjured-items.txt").getFile();

        System.out.println("OMGHAI!");

        GenericGildedRoseItem[] items = new GenericGildedRoseItem[]{
            new GenericGildedRoseItem("+5 Dexterity Vest", 10, 20), //
            new AgedBrieItem("Aged Brie", 2, 0), //
            new GenericGildedRoseItem("Elixir of the Mongoose", 5, 7), //
            new SulfurasItem("Sulfuras, Hand of Ragnaros", 0, 80), //
            new SulfurasItem("Sulfuras, Hand of Ragnaros", -1, 80),
            new BackstagePassItem("Backstage passes to a TAFKAL80ETC concert", 15, 20),
            new BackstagePassItem("Backstage passes to a TAFKAL80ETC concert", 10, 49),
            new BackstagePassItem("Backstage passes to a TAFKAL80ETC concert", 5, 49),
            // this conjured item does not work properly yet
            new ConjuredItem("Conjured Mana Cake", 3, 6)};

        GildedRose app = new GildedRose(items);

        int days = 20;

        for (int i = 0; i < days; i++) {
            System.out.println("-------- day " + i + " --------");
            System.out.println("name, sellIn, quality");
            for (Item item : items) {
                System.out.println(item);
            }
            System.out.println();
            app.updateQuality();
        }

        String testRun = new String(baos.toByteArray());
        String goldenMaster = StreamUtils.copyToString(new FileInputStream(new File(masterFile)), Charset.defaultCharset());

        assertEquals(goldenMaster, testRun);
    }
}
