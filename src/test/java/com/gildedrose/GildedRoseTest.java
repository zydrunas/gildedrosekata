package com.gildedrose;

import static org.junit.Assert.*;

import com.gildedrose.entity.*;
import org.junit.Test;

public class GildedRoseTest {

    private GildedRose app;

    @Test
    public void testGildedRoseItemQualityUpdate() {
        GenericGildedRoseItem genericGildedRoseItem = new GenericGildedRoseItem("Elixir of the Mongoose", 2, 3);
        fastForward(genericGildedRoseItem, 1);
        checkUpdatedItemValues(genericGildedRoseItem, 1, 2);
    }

    @Test
    public void testLegendaryItemQualityFixed() {
        SulfurasItem sulfurasItem = new SulfurasItem("Sulfuras, Hand of Ragnaros", -1, 80);
        fastForward(sulfurasItem, 50);
        checkUpdatedItemValues(sulfurasItem, -1, 80);
    }

    @Test
    public void testGildedRoseQualityAlwaysAboveZero() {
        GenericGildedRoseItem genericGildedRoseItem = new GenericGildedRoseItem("Elixir of the Mongoose", 5, 7);
        fastForward(genericGildedRoseItem, 7);
        checkUpdatedItemValues(genericGildedRoseItem, -2, 0);
    }

    @Test
    public void testDoubleDegradationOnExpire() {
        GenericGildedRoseItem genericGildedRoseItem = new GenericGildedRoseItem("Foo", 1, 10);
        fastForward(genericGildedRoseItem, 4);
        checkUpdatedItemValues(genericGildedRoseItem, -3, 3);
    }

    @Test
    public void testBackstagePassQualityDrop() {
        BackstagePassItem backstagePassItem = new BackstagePassItem("Backstage passes to a TAFKAL80ETC concert", 9, 30);
        fastForward(backstagePassItem, 10);
        checkUpdatedItemValues(backstagePassItem, -1, 0);
    }

    @Test
    public void testBackstagePassQualityIncrease() {
        BackstagePassItem backstagePassItem = new BackstagePassItem("Backstage passes to a TAFKAL80ETC concert", 9, 30);

        // Increase by 2
        fastForward(backstagePassItem, 2);
        checkUpdatedItemValues(backstagePassItem, 7, 34);

        // Increase by 3
        fastForward(backstagePassItem, 3);
        checkUpdatedItemValues(backstagePassItem, 4, 41);
    }

    @Test
    public void testGildedRoseItemIntegrity() {
        String itemTargetName = "Sulfuras, Hand of Ragnaros, -1, 80";

        SulfurasItem sulfurasItem = new SulfurasItem("Sulfuras, Hand of Ragnaros", -1, 80);
        fastForward(sulfurasItem, 1);
        assertEquals("toString() method: ", itemTargetName, sulfurasItem.toString());
    }

    @Test
    public void testConjuredItem() {
        ConjuredItem conjuredItem = new ConjuredItem("Conjured Mana Cake", 10, 10);
        fastForward(conjuredItem, 2);
        checkUpdatedItemValues(conjuredItem, 8, 6);
    }

    @Test
    public void testItemQuality() {
        AgedBrieItem agedBrieItem = new AgedBrieItem("Aged Brie", 2, 50);
        fastForward(agedBrieItem, 2);
        checkUpdatedItemValues(agedBrieItem, 0, 50);
    }

    @Test
    public void testAgedBrieItem() {
        AgedBrieItem agedBrieItem = new AgedBrieItem("Aged Brie", 5, 5);
        fastForward(agedBrieItem, 7);
        checkUpdatedItemValues(agedBrieItem, -2, 14);
    }

    /**
     * Used to fas-forward days
     *
     * @param genericGildedRoseItem GenericGildedRoseItem
     * @param days                  int
     */
    private void fastForward(GenericGildedRoseItem genericGildedRoseItem, int days) {
        if (days > 0) {
            for (int i = 0; i < days; i++) {
                genericGildedRoseItem.updateItemQuality();
            }
        }
    }

    /**
     * Assert item properties
     *
     * @param genericGildedRoseItem GenericGildedRoseItem
     * @param sellIn                int
     * @param quality               int
     */
    private void checkUpdatedItemValues(GenericGildedRoseItem genericGildedRoseItem, int sellIn, int quality) {
        assertEquals("sellIn", sellIn, genericGildedRoseItem.sellIn);
        assertEquals("quality", quality, genericGildedRoseItem.quality);
    }
}
