package com.gildedrose;

import com.gildedrose.entity.GenericGildedRoseItem;
import com.gildedrose.repository.GildedRoseRepository;
import org.junit.*;
import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class GildedRoseRepositoryTest {

    @Autowired
    private GildedRoseRepository gildedRoseRepository;

    @Before
    public void setUp() {
        gildedRoseRepository = new GildedRoseRepository();
    }

    @Test
    public void testFindAll() {
        List<GenericGildedRoseItem> genericGildedRoseItems = gildedRoseRepository.findAll();
        assertEquals(9, genericGildedRoseItems.size());
    }

    @Test
    public void testFindById() {
        List<GenericGildedRoseItem> genericGildedRoseItems = gildedRoseRepository.findAll();
        GenericGildedRoseItem genericGildedRoseItem = genericGildedRoseItems.get(0);

        GenericGildedRoseItem foundItem = gildedRoseRepository.findById(genericGildedRoseItem.getId());

        assertEquals(genericGildedRoseItem.name, foundItem.name);
        assertEquals(genericGildedRoseItem.quality, foundItem.quality);
        assertEquals(genericGildedRoseItem.sellIn, foundItem.sellIn);
    }
}
