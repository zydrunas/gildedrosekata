package com.gildedrose.controller;

import com.gildedrose.entity.GenericGildedRoseItem;
import com.gildedrose.repository.GildedRoseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GildedRoseController {

    @Autowired
    private GildedRoseRepository gildedRoseRepository;

    @GetMapping(value = "/api/item/all")
    @ResponseBody
    public List<GenericGildedRoseItem> getAllItems() {
        return gildedRoseRepository.findAll();
    }

    @GetMapping(value = "/api/item/{itemid}")
    @ResponseBody
    public GenericGildedRoseItem getItem(@PathVariable("itemid") String itemid) {
        return gildedRoseRepository.findById(itemid);
    }

    @PostMapping(value = "/api/item/update")
    @ResponseBody
    public String updateItems() {
        gildedRoseRepository.findAll().forEach(GenericGildedRoseItem::updateItemQuality);

        return "OK";
    }
}
