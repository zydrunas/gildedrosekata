package com.gildedrose;

import com.gildedrose.entity.GenericGildedRoseItem;

import java.util.Arrays;
import java.util.List;

class GildedRose {

    // An array of type item
    private GenericGildedRoseItem[] items;

    /**
     * @param items GenericGildedRoseItem[]
     */
    public GildedRose(GenericGildedRoseItem[] items) {
        this.items = items;
    }

    /**
     * The main method from the legacy source
     */
    public void updateQuality() {
        List<GenericGildedRoseItem> itemList = Arrays.asList(this.items);

        try {
            itemList.forEach(GenericGildedRoseItem::updateItemQuality);
        } catch (Exception e) {
            System.out.println("Exception thrown: " + e);
            throw e;
        }
    }
}