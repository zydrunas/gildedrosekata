package com.gildedrose.entity;

public class Item {

    private static final int MAX_QUALITY = 50;

    public String name;

    public int sellIn;

    public int quality;

    public Item(String name, int sellIn, int quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }

    @Override
    public String toString() {
        return this.name + ", " + this.sellIn + ", " + this.quality;
    }

    protected void increaseQuality() {
        if (this.quality < MAX_QUALITY) {
            this.quality++;
        }
    }

    protected void decreaseQuality() {
        if (this.quality > 0) {
            this.quality--;
        }
    }
}
