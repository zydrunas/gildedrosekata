package com.gildedrose.entity;

public class ConjuredItem extends GenericGildedRoseItem {

    public ConjuredItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateItemQuality() {
        this.sellIn--;

        this.decreaseQuality();
        this.decreaseQuality();
    }
}
