package com.gildedrose.entity;

public class SulfurasItem extends GenericGildedRoseItem {

    public SulfurasItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateItemQuality() {
        // Sulfuras item should not modify parent obejct properties
    }
}
