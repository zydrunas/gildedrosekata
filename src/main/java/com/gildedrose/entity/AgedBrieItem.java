package com.gildedrose.entity;

public class AgedBrieItem extends GenericGildedRoseItem {

    public AgedBrieItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateItemQuality() {
        this.sellIn--;

        this.increaseQuality();

        if (this.sellIn < 0) {
            this.increaseQuality();
        }
    }
}
