package com.gildedrose.entity;

import com.gildedrose.interfaces.GildedRoseItemInterface;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.UUID;

public class GenericGildedRoseItem extends Item implements GildedRoseItemInterface {

    private String id;

    public GenericGildedRoseItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);

        this.setId(this.generateUUID());
    }

    @Override
    public void updateItemQuality() {
        this.sellIn--;

        this.decreaseQuality();

        if (this.sellIn < 0) {
            this.decreaseQuality();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String generateUUID() {
        try {
            MessageDigest salt = MessageDigest.getInstance("SHA-256");
            salt.update(UUID.randomUUID().toString().getBytes("UTF-8"));

            return bytesToHex(salt.digest());
        } catch (Exception e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
        }

        return null;
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for (byte b : bytes) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }
}
