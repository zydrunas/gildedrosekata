package com.gildedrose.entity;

public class BackstagePassItem extends GenericGildedRoseItem {

    private static final int SELLIN_FOR_QUALITY_MOD2 = 11;

    private static final int SELLIN_FOR_QUALITY_MOD3 = 6;

    public BackstagePassItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateItemQuality() {
        this.increaseQuality();

        if (this.sellIn < SELLIN_FOR_QUALITY_MOD2) {
            this.increaseQuality();
        }

        if (this.sellIn < SELLIN_FOR_QUALITY_MOD3) {
            this.increaseQuality();
        }

        this.sellIn--;

        if (this.sellIn < 0) {
            this.quality = 0;
        }
    }
}
