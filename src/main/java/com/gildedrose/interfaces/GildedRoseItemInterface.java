package com.gildedrose.interfaces;

public interface GildedRoseItemInterface {
    public void updateItemQuality();
}
