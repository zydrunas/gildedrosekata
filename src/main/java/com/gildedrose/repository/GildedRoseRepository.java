package com.gildedrose.repository;

import com.gildedrose.entity.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Repository
public class GildedRoseRepository {
    private List<GenericGildedRoseItem> itemsList = Arrays.asList(
        new GenericGildedRoseItem("+5 Dexterity Vest", 10, 20),
        new AgedBrieItem("Aged Brie", 2, 0),
        new GenericGildedRoseItem("Elixir of the Mongoose", 5, 7),
        new SulfurasItem("Sulfuras, Hand of Ragnaros", 0, 80),
        new SulfurasItem("Sulfuras, Hand of Ragnaros", -1, 80),
        new BackstagePassItem("Backstage passes to a TAFKAL80ETC concert", 15, 20),
        new BackstagePassItem("Backstage passes to a TAFKAL80ETC concert", 10, 49),
        new BackstagePassItem("Backstage passes to a TAFKAL80ETC concert", 5, 49),
        new ConjuredItem("Conjured Mana Cake", 3, 6)
    );

    public List<GenericGildedRoseItem> findAll() {
        return new ArrayList<>(itemsList);
    }

    public GenericGildedRoseItem findById(String id) {
        Optional<GenericGildedRoseItem> genericGildedRoseItem = itemsList.stream()
                .filter(item -> item.getId().equals(id))
                .findFirst();

        return genericGildedRoseItem.orElse(null);
    }
}
